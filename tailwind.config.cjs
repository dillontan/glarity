/** @type {import('tailwindcss').Config} */
module.exports = {
  // This adds a prefix to all Tailwind CSS utility classes to avoid conflicts.
  // All Tailwind classes will be prefixed with glarity--.
  // For example: bg-blue-500 will become glarity--bg-blue-500
  prefix: 'glarity--',

  // This option disables the Preflight base styles provided by Tailwind CSS.
  corePlugins: {
    preflight: false,
  },

  // This specifies the paths to all of your template files.
  // Tailwind CSS will scan these files for class names to include in the final CSS.
  content: ['./src/**/*.tsx'],

  // This is where you extend or customize the default Tailwind theme.
  theme: {
    extend: {},
  },

  // Allows manual toggling of dark mode instead of relying on OS preference.
  darkMode: 'selector',

  // This is where you can add custom plugins to extend Tailwind's functionality.
  plugins: [],
}
