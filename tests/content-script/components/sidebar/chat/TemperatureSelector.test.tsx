import { fireEvent, render, screen } from '@testing-library/preact'
import TemperatureSelector from '../../../../../src/content-script/components/sidebar/chat/TemperatureSelector'

describe('TemperatureSelector', () => {
  it('renders buttons with initial state', () => {
    render(<TemperatureSelector />)

    expect(screen.getByText('Creative')).toBeInTheDocument()
    expect(screen.getByText('Balanced')).toBeInTheDocument()
    expect(screen.getByText('Precise')).toBeInTheDocument()
  })

  it('has Balanced selected initially', () => {
    render(<TemperatureSelector />)

    const balancedButton = screen.getByText('Balanced')
    expect(balancedButton).toHaveClass('glarity--selected')
  })

  it('changes selection when buttons are clicked', () => {
    render(<TemperatureSelector />)

    const creativeButton = screen.getByText('Creative')
    const balancedButton = screen.getByText('Balanced')
    fireEvent.click(creativeButton)

    expect(creativeButton).toHaveClass('glarity--selected')
    expect(balancedButton).not.toHaveClass('glarity--selected')
  })

  it('plays correct animation when selection changes from left to right', () => {
    render(<TemperatureSelector />)

    const balancedButton = screen.getByText('Balanced')
    const preciseButton = screen.getByText('Precise')

    fireEvent.click(preciseButton)

    expect(balancedButton).toHaveClass('sweep-out-right')
    expect(preciseButton).toHaveClass('sweep-in-left')
  })

  it('plays correct animation when selection changes from right to left', () => {
    render(<TemperatureSelector />)

    const creativeButton = screen.getByText('Creative')
    const balancedButton = screen.getByText('Balanced')

    fireEvent.click(creativeButton)

    expect(creativeButton).toHaveClass('sweep-in-right')
    expect(balancedButton).toHaveClass('sweep-out-left')
  })

  it('clears timeout on unmount', () => {
    const { unmount } = render(<TemperatureSelector />)

    unmount()
    // Test if the component unmounts without errors
    expect(screen.queryByText('Creative')).toBeNull()
  })
})
