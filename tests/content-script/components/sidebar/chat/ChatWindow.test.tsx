import { render, screen } from '@testing-library/preact'
import ChatWindow from '../../../../../src/content-script/components/sidebar/chat/ChatWindow'

const USER_MESSAGE = 'user message'
const BOT_MESSAGE = 'bot message'

const messages = [
  { text: USER_MESSAGE, user: 'user' },
  { text: BOT_MESSAGE, user: 'bot' },
]

describe('ChatWindow', () => {
  it('renders messages correctly', () => {
    render(<ChatWindow messages={messages} />)

    expect(screen.getByText(USER_MESSAGE)).toBeInTheDocument()
    expect(screen.getByText(BOT_MESSAGE)).toBeInTheDocument()
  })

  it('applies correct classes to user and bot messages', () => {
    render(<ChatWindow messages={messages} />)

    const userMessage = screen.getByText(USER_MESSAGE)
    const botMessage = screen.getByText(BOT_MESSAGE)

    const userMessageStyleClass = 'glarity--bg-green-500'
    const botMessageStyleClass = 'glarity--bg-blue-500'

    expect(userMessage).toHaveClass(userMessageStyleClass)
    expect(botMessage).toHaveClass(botMessageStyleClass)
  })
})
