import { fireEvent, render, screen } from '@testing-library/preact'
import ChatUi from '../../../../../src/content-script/components/sidebar/chat/ChatUi'

describe('ChatUi', () => {
  it('renders ChatWindow, TemperatureSelector, and ChatBox components', () => {
    render(<ChatUi />)

    expect(screen.getByTestId('ChatWindow')).toBeInTheDocument()
    expect(screen.getByPlaceholderText('Type a message...')).toBeInTheDocument()
    expect(screen.getByText('Balanced')).toBeInTheDocument()
  })

  it('shows messages sent by the user in the ChatWindow', async () => {
    render(<ChatUi />)

    const input = screen.getByPlaceholderText('Type a message...')
    const sendButton = screen.getByText('Send')

    fireEvent.change(input, { target: { value: 'Hello' } })
    fireEvent.click(sendButton)

    expect(screen.getByText('Hello')).toBeInTheDocument()
  })
})
