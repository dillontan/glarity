import { fireEvent, render, screen } from '@testing-library/preact'
import ChatBox from '../../../../../src/content-script/components/sidebar/chat/ChatBox'

const INPUT_PLACEHOLDER_TEXT = 'Type a message...'
const SEND_BUTTON_TEXT = 'Send'

describe('ChatBox', () => {
  it('renders input and send button', () => {
    render(<ChatBox onSendMessage={() => {}} />)

    expect(screen.getByPlaceholderText(INPUT_PLACEHOLDER_TEXT)).toBeInTheDocument()
    expect(screen.getByText(SEND_BUTTON_TEXT)).toBeInTheDocument()
  })

  it('calls onSendMessage when send button is clicked', () => {
    const onSendMessage = vi.fn()
    render(<ChatBox onSendMessage={onSendMessage} />)

    const input = screen.getByPlaceholderText(INPUT_PLACEHOLDER_TEXT)
    const sendButton = screen.getByText(SEND_BUTTON_TEXT)

    fireEvent.change(input, { target: { value: 'Test message' } })
    fireEvent.click(sendButton)

    expect(onSendMessage).toHaveBeenCalledWith('Test message')
  })

  it('clears input after sending a message', () => {
    const onSendMessage = vi.fn()
    render(<ChatBox onSendMessage={onSendMessage} />)

    const input = screen.getByPlaceholderText(INPUT_PLACEHOLDER_TEXT)
    const sendButton = screen.getByText(SEND_BUTTON_TEXT)

    fireEvent.change(input, { target: { value: 'Test message' } })
    fireEvent.click(sendButton)

    expect(input.value).toBe('')
  })
})
