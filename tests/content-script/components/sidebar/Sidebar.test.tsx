import { fireEvent, render, screen } from '@testing-library/preact'
import Sidebar from '../../../../src/content-script/components/sidebar/Sidebar'

describe('Sidebar', () => {
  it('renders initial content correctly', () => {
    render(<Sidebar toggleSidebar={() => {}} />)

    expect(screen.getByText('Home')).toBeInTheDocument()
    expect(screen.getByTestId('NavigationBar')).toBeInTheDocument()
  })

  it('switches content when navigation buttons are clicked', () => {
    render(<Sidebar toggleSidebar={() => {}} />)

    fireEvent.click(screen.getByTestId('chatButton'))
    expect(screen.getByPlaceholderText('Type a message...')).toBeInTheDocument()

    fireEvent.click(screen.getByTestId('summaryButton'))
    expect(screen.getByText('Summary')).toBeInTheDocument()

    fireEvent.click(screen.getByTestId('homeButton'))
    expect(screen.getByText('Home')).toBeInTheDocument()
  })
})
