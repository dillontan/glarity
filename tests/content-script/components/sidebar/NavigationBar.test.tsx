import { fireEvent, render, screen } from '@testing-library/preact'
import Browser from 'webextension-polyfill'
import NavigationBar from '../../../../src/content-script/components/sidebar/NavigationBar'

describe('NavigationBar', () => {
  it('renders buttons correctly', () => {
    render(<NavigationBar setSidebarContent={() => {}} toggleSidebar={() => {}} />)
    expect(screen.getByTestId('closeButton')).toBeInTheDocument()
    expect(screen.getByTestId('homeButton')).toBeInTheDocument()
    expect(screen.getByTestId('summaryButton')).toBeInTheDocument()
    expect(screen.getByTestId('chatButton')).toBeInTheDocument()
    expect(screen.getByLabelText('Settings')).toBeInTheDocument()
  })

  it('calls openOptionsPage when cogwheel icon is clicked', () => {
    render(<NavigationBar setSidebarContent={vi.fn()} toggleSidebar={vi.fn()} />)

    const settingsIcon = screen.getByLabelText('Settings')
    fireEvent.click(settingsIcon)

    expect(Browser.runtime.sendMessage).toHaveBeenCalledWith({ type: 'OPEN_OPTIONS_PAGE' })
  })

  it('calls setSidebarContent with correct arguments when buttons are clicked', () => {
    const setSidebarContent = vi.fn()
    render(<NavigationBar setSidebarContent={setSidebarContent} toggleSidebar={vi.fn()} />)

    fireEvent.click(screen.getByTestId('homeButton'))
    expect(setSidebarContent).toHaveBeenCalledWith('home')

    fireEvent.click(screen.getByTestId('summaryButton'))
    expect(setSidebarContent).toHaveBeenCalledWith('summary')

    fireEvent.click(screen.getByTestId('chatButton'))
    expect(setSidebarContent).toHaveBeenCalledWith('chat')
  })

  it('calls toggleSidebar when close button is clicked', () => {
    const toggleSidebar = vi.fn()
    render(<NavigationBar setSidebarContent={() => {}} toggleSidebar={toggleSidebar} />)

    fireEvent.click(screen.getByTestId('closeButton'))
    expect(toggleSidebar).toHaveBeenCalled()
  })
})
