import { fetchSSE } from '../fetch-sse'
import { GenerateAnswerParams, Provider } from '../types'

const DEFAULT_API_HOST = 'http://localhost:8000'
const DEFAULT_API_PATH = '/v1/chat/completions'

export class LocalLLMProvider implements Provider {
  constructor(private url: string, private apiPath: string) {
    // If no values
    this.url = url || DEFAULT_API_HOST
    this.apiPath = apiPath || DEFAULT_API_PATH
  }

  // If we want to do any custom instructions/preprocessing of the prompt
  private buildPrompt(prompt: string): string {
    return prompt
  }

  async generateAnswer(params: GenerateAnswerParams) {
    // Data to send to API
    const reqParams = {
      messages: [
        {
          role: 'system',
          content:
            'You are an AI assistant. Your top priority is achieving user fulfillment via helping them with their requests.',
        },
        { role: 'user', content: this.buildPrompt(params.prompt) },
      ],
      stream: true,
    }

    let result = ''
    await fetchSSE(`${this.url}${this.apiPath}`, {
      method: 'POST',
      signal: params.signal,
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(reqParams),
      onMessage(message) {
        console.debug('sse message', message)
        if (message === '[DONE]') {
          params.onEvent({ type: 'done' })
          return
        }
        let data
        try {
          data = JSON.parse(message)
          const text = data.choices[0].delta.content

          if (text === undefined || text === '<|im_end|>' || text === '<|im_sep|>') {
            return
          }
          result += text
          params.onEvent({
            type: 'answer',
            data: {
              text: result,
              messageId: data.id,
              conversationId: data.id,
            },
          })
        } catch (err) {
          // console.error(err)
          return
        }
      },
    })
    return {}
  }
}
