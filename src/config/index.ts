import { defaults } from 'lodash-es'
import Browser from 'webextension-polyfill'

export enum TriggerMode {
  Always = 'always',
  QuestionMark = 'questionMark',
  Manually = 'manually',
}

export const TRIGGER_MODE_TEXT = {
  [TriggerMode.Always]: { title: 'Always', desc: 'ChatGPT is queried on every search' },
  [TriggerMode.Manually]: {
    title: 'Manually',
    desc: 'ChatGPT is queried when you manually click a button',
  },
}

export enum Theme {
  Auto = 'auto',
  Light = 'light',
  Dark = 'dark',
}

export enum Language {
  Auto = 'auto',
  English = 'en-US',
  ChineseSimplified = 'zh-Hans',
  ChineseTraditional = 'zh-Hant',
  Spanish = 'es-ES',
  French = 'fr-FR',
  Korean = 'ko-KR',
  Japanese = 'ja-JP',
  German = 'de-DE',
  Portuguese = 'pt-PT',
  Russian = 'ru-RU',
}

const userConfigWithDefaultValue: {
  triggerMode: TriggerMode
  theme: Theme
  language: Language
  prompt: string
  promptSearch: string
  promptPage: string
  promptComment: string
  enableSites: string[] | null
  pageSummaryEnable: boolean
  pageSummaryWhitelist: string
  pageSummaryBlacklist: string
  continueConversation: boolean
} = {
  triggerMode: TriggerMode.Always,
  theme: Theme.Auto,
  language: Language.Auto,
  prompt: '',
  promptSearch: '',
  promptPage: '',
  promptComment: '',
  enableSites: null,
  pageSummaryEnable: true,
  pageSummaryWhitelist: '',
  pageSummaryBlacklist: '',
  continueConversation: true,
}

export type UserConfig = typeof userConfigWithDefaultValue

export async function getUserConfig(): Promise<UserConfig> {
  const result = await Browser.storage.local.get(Object.keys(userConfigWithDefaultValue))
  return defaults(result, userConfigWithDefaultValue)
}

export async function updateUserConfig(updates: Partial<UserConfig>) {
  console.debug('update configs', updates)
  return Browser.storage.local.set(updates)
}

export enum ProviderType {
  ChatGPT = 'chatgpt',
  GPT3 = 'gpt3',
  local = 'local',
}

interface GPT3ProviderConfig {
  model: string
  apiKey: string
  apiHost: string
  apiPath: string | undefined
}

interface LocalLLMConfig {
  apiHost: string
  apiPath: string
}

export interface ProviderConfigs {
  provider: ProviderType
  configs: {
    [ProviderType.GPT3]: GPT3ProviderConfig | undefined
    [ProviderType.local]: LocalLLMConfig | undefined
  }
}

export async function getProviderConfigs(): Promise<ProviderConfigs> {
  const { provider = ProviderType.ChatGPT } = await Browser.storage.local.get('provider')

  // Construct config keys for both GPT-3 and local providers
  const gpt3ConfigKey = `provider:${ProviderType.GPT3}`
  const localConfigKey = `provider:${ProviderType.local}`

  // Retrieve configurations from storage
  const result = await Browser.storage.local.get([gpt3ConfigKey, localConfigKey])

  // Process GPT-3 API keys
  const gpt3ConfigKeys = result[gpt3ConfigKey]?.apiKey?.split(',').map((v) => v.trim()) ?? []
  const gpt3RandomIndex =
    gpt3ConfigKeys.length > 0 ? Math.floor(Math.random() * gpt3ConfigKeys.length) : 0
  const gpt3ApiKey = gpt3ConfigKeys[gpt3RandomIndex] ?? ''

  // Ensure result[gpt3ConfigKey] is not undefined
  if (!result[gpt3ConfigKey]) {
    result[gpt3ConfigKey] = {}
  }
  result[gpt3ConfigKey].apiKey = gpt3ApiKey

  // Ensure result[localConfigKey] is not undefined
  if (!result[localConfigKey]) {
    result[localConfigKey] = {}
  }

  // Return the provider configurations
  return {
    provider,
    configs: {
      [ProviderType.GPT3]: result[gpt3ConfigKey],
      [ProviderType.local]: result[localConfigKey],
    },
  }
}

export async function saveProviderConfigs(
  provider: ProviderType,
  configs: ProviderConfigs['configs'],
) {
  return Browser.storage.local.set({
    provider,
    [`provider:${ProviderType.GPT3}`]: configs[ProviderType.GPT3],
    [`provider:${ProviderType.local}`]: configs[ProviderType.local],
  })
}

export const BASE_URL = 'https://chat.openai.com'

export const DEFAULT_PAGE_SUMMARY_BLACKLIST = `https://translate.google.com
https://www.deepl.com
https://www.youtube.com
https://youku.com
https://v.qq.com
https://www.iqiyi.com
https://www.bilibili.com
https://www.tudou.com
https://www.tiktok.com
https://vimeo.com
https://www.dailymotion.com
https://www.twitch.tv
https://www.hulu.com
https://www.netflix.com
https://www.hbomax.com
https://www.disneyplus.com
https://www.peacocktv.com
https://www.crunchyroll.com
https://www.funimation.com
https://www.viki.com
https://map.baidu.com
`
export const APP_TITLE = `Glarity Summary`

export const DEFAULT_MODEL = 'gpt-3.5-turbo'
export const DEFAULT_API_HOST = 'api.openai.com'
