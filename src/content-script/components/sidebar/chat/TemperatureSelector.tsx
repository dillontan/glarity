import { useEffect, useState } from 'react'

const TemperatureSelector = () => {
  const [selected, setSelected] = useState('Balanced')
  const [prevSelected, setPrevSelected] = useState('Balanced')

  // At the end of the 500ms animation, update the class to the static background
  useEffect(() => {
    const timer = setTimeout(() => handleSelect(selected), 500)
    return () => clearTimeout(timer)
  }, [selected])

  // Function that fires when new option is selected
  const handleSelect = (option) => {
    setPrevSelected(selected)
    setSelected(option)
  }

  // Gets the direction of option change for the animation
  // Creative -> Balanced/Precise = Right, vice versa
  const getDirection = () => {
    const order = ['Creative', 'Balanced', 'Precise']

    if (order.indexOf(selected) > order.indexOf(prevSelected)) {
      return 'right'
    } else {
      return 'left'
    }
  }

  // Determines which animation to play for the button
  // This is run for each button every time any button is pressed
  const getButtonClass = (option) => {
    // Base button style
    const baseClass = 'glarity--px-4 glarity--py-2 glarity--rounded-full '

    const direction = getDirection()
    const oppositeDirection = direction === 'left' ? 'right' : 'left'

    // e.g. switching from Creative to Balanced:
    // Direction: right
    // Creative should play sweep-out-right
    // Balanced should play sweep-in-left
    if (option === selected) {
      return `${baseClass} glarity--selected ${
        option !== prevSelected ? `sweep-in-${oppositeDirection}` : ''
      }`
    } else if (option === prevSelected) {
      return `${baseClass} ${`sweep-out-${direction}`}`
    } else {
      return `${baseClass} glarity--unselected`
    }
  }

  return (
    <div className="glarity--flex glarity--justify-center glarity--items-center glarity--pt-4 glarity--space-x-2">
      <button className={getButtonClass('Creative')} onClick={() => handleSelect('Creative')}>
        Creative
      </button>
      <button className={getButtonClass('Balanced')} onClick={() => handleSelect('Balanced')}>
        Balanced
      </button>
      <button className={getButtonClass('Precise')} onClick={() => handleSelect('Precise')}>
        Precise
      </button>
    </div>
  )
}

export default TemperatureSelector
