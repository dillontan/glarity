import { useState } from 'react'
import ChatBox from './ChatBox'
import ChatWindow from './ChatWindow'
import TemperatureSelector from './TemperatureSelector'

type Message = {
  text: string
  user: 'user' | 'bot'
}

const ChatUi = () => {
  const [messages, setMessages] = useState<Message[]>([])

  const handleSendMessage = (message: string) => {
    setMessages([...messages, { text: message, user: 'user' }])

    // Simulate bot response
    setTimeout(() => {
      setMessages((prevMessages) => {
        const botResponseText =
          prevMessages.length > 0
            ? prevMessages[prevMessages.length - 1].text
            : 'Default bot response'

        return [...prevMessages, { text: botResponseText, user: 'bot' }]
      })
    }, 1000)
  }

  return (
    <div className="glarity--flex glarity--flex-col glarity--w-full glarity--h-screen glarity--justify-between">
      <ChatWindow messages={messages} />

      <TemperatureSelector />

      <div className="glarity--py-4">
        <hr className="glarity--border-t-2 glarity--border-slate-800 dark:glarity--border-gray-300" />
      </div>

      <ChatBox onSendMessage={handleSendMessage} />
    </div>
  )
}
export default ChatUi
