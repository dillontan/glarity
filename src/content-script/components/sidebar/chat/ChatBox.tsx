import { useState } from 'react'

const ChatBox = ({ onSendMessage }) => {
  const [inputValue, setInputValue] = useState('')

  const handleSubmit = (e) => {
    e.preventDefault()
    if (inputValue.trim()) {
      onSendMessage(inputValue)
      setInputValue('')
    }
  }

  return (
    <form onSubmit={handleSubmit} className="glarity--flex glarity--pb-4">
      <input
        type="text"
        value={inputValue}
        onChange={(e) => setInputValue(e.target.value)}
        placeholder="Type a message..."
        className="glarity--flex-1 glarity--p-2 glarity--mr-2 glarity--rounded glarity--bg-slate-200 glarity--text-black glarity--placeholder-gray-400 dark:glarity--bg-gray-800 dark:glarity--text-white dark:glarity--placeholder-gray-600"
      />
      <button
        type="submit"
        className="glarity--bg-blue-500 glarity--text-white glarity--p-2 glarity--rounded"
      >
        Send
      </button>
    </form>
  )
}

export default ChatBox
