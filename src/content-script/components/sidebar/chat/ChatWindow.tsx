const ChatWindow = ({ messages }) => {
  return (
    <div
      data-testid="ChatWindow"
      className="glarity--flex-1 glarity--overflow-y-auto glarity--overflow-x-hidden glarity--space-y-2 glarity--max-w-full"
    >
      {messages.map((message, index) => (
        <div key={index} className="glarity--flex glarity--w-full">
          {/* Container to push the chat bubble to the left/right for bot/user respectively */}
          <div
            className={`glarity--grow glarity--px-4 ${
              message.user === 'bot' ? 'glarity--order-last' : ''
            }`}
          ></div>

          <div
            className={`glarity--p-2 glarity--rounded-lg glarity--inline-block glarity--break-all glarity--max-w-sm ${
              // User bubbles are green, bot bubbles are blue
              message.user === 'bot'
                ? 'glarity--bg-blue-500 glarity--text-white glarity--mr-auto'
                : 'glarity--bg-green-500 glarity--text-white'
            }`}
          >
            {message.text}
          </div>
        </div>
      ))}
    </div>
  )
}

export default ChatWindow
