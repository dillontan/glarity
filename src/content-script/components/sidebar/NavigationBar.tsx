import {
  ChatBubbleOvalLeftEllipsisIcon,
  Cog6ToothIcon,
  DocumentTextIcon,
  HomeIcon,
  XMarkIcon,
} from '@heroicons/react/24/outline'
import PropType from 'prop-types'
import Browser from 'webextension-polyfill'

const openOptionsPage = () => {
  Browser.runtime.sendMessage({ type: 'OPEN_OPTIONS_PAGE' })
}

const NavigationBar = ({ setSidebarContent, toggleSidebar }) => {
  const iconSize = 'glarity--w-6 glarity--h-6'
  const iconStyle = 'glarity--text-slate-800 dark:glarity--text-white'
  const iconPadding = 'glarity--my-2'

  return (
    <div
      data-testid="NavigationBar"
      className="glarity--bg-slate-200 dark:glarity--bg-gray-800 glarity--text-white glarity--flex glarity--flex-col glarity--items-center glarity--p-4"
    >
      <button data-testid="closeButton" onClick={toggleSidebar}>
        <XMarkIcon className={`${iconSize} ${iconStyle}`} />
      </button>

      <hr className="glarity--w-full glarity--my-2 glarity--border-t glarity--border-gray-500" />

      <div className="glarity--flex glarity--flex-col glarity--flex-grow glarity--items-center glarity--w-full">
        <button data-testid="homeButton" onClick={() => setSidebarContent('home')}>
          <HomeIcon className={`${iconSize} ${iconStyle} ${iconPadding}`} />
        </button>

        <button data-testid="summaryButton" onClick={() => setSidebarContent('summary')}>
          <DocumentTextIcon className={`${iconSize} ${iconStyle} ${iconPadding}`} />
        </button>

        <button data-testid="chatButton" onClick={() => setSidebarContent('chat')}>
          <ChatBubbleOvalLeftEllipsisIcon className={`${iconSize} ${iconStyle} ${iconPadding}`} />
        </button>
      </div>

      <button>
        <Cog6ToothIcon
          aria-label="Settings"
          className={`${iconSize} ${iconStyle} ${iconPadding}`}
          onClick={openOptionsPage}
        />
      </button>
    </div>
  )
}

NavigationBar.propTypes = {
  setSidebarContent: PropType.func.isRequired,
  toggleSidebar: PropType.func.isRequired,
}

export default NavigationBar
