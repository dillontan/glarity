import { getUserConfig, Theme } from '@/config'
import { detectSystemColorScheme } from '@/utils/utils'
import { useEffect, useState } from 'react'
import ChatUi from './chat/ChatUi'
import NavigationBar from './NavigationBar'

const Sidebar = ({ toggleSidebar }) => {
  const [sidebarContent, setSidebarContent] = useState('home')
  const [theme, setTheme] = useState<Theme>(Theme.Dark)

  /**
   * Returns the content to be loaded in the sidebar based on the state of sidebarContent.
   */
  const loadSidebarContent = () => {
    switch (sidebarContent) {
      case 'home':
        return <div className="glarity--text-black dark:glarity--text-white">Home</div>
      case 'chat':
        return <ChatUi />
      case 'summary':
        return <div className="glarity--text-black dark:glarity--text-white">Summary</div>
    }
  }

  /**
   * Fetches the user configuration asynchronously and sets the theme accordingly.
   *
   */
  useEffect(() => {
    const fetchUserConfig = async () => {
      const userConfig = await getUserConfig()
      if (userConfig.theme === Theme.Auto) {
        setTheme(detectSystemColorScheme())
      } else {
        setTheme(userConfig.theme)
      }
    }

    fetchUserConfig()
  }, [])

  return (
    <>
      <div className={theme === Theme.Dark ? 'glarity--dark' : ''}>
        {/* Main container of the sidebar */}
        <div className="glarity--fixed glarity--top-0 glarity--right-0 glarity--w-1/4 glarity--h-full glarity--bg-slate-200 dark:glarity--bg-gray-900 glarity--text-white glarity--flex glarity--bg-opacity-90">
          {/* Container for the content of the sidebar */}
          <div className="glarity--w-full glarity--flex glarity--flex-col glarity--justify-center glarity--items-center glarity--overflow-hidden glarity--p-4">
            {loadSidebarContent()}
          </div>
          <NavigationBar setSidebarContent={setSidebarContent} toggleSidebar={toggleSidebar} />
        </div>{' '}
      </div>
    </>
  )
}

export default Sidebar
