import ChatGPTQuery from '@/content-script/components/ChatGPTQuery'
import { GearIcon, XCircleFillIcon } from '@primer/octicons-react'
import classNames from 'classnames'
import { useCallback, useEffect, useState } from 'preact/hooks'
import Browser from 'webextension-polyfill'
// import { extractFromHtml } from '@/utils/article-extractor/cjs/article-extractor.esm'
import logoWhite from '@/assets/img/logo-white.png'
import logo from '@/assets/img/logo.png'
import { APP_TITLE, getProviderConfigs, getUserConfig, Language } from '@/config'
import { getSummaryPrompt } from '@/content-script/prompt'
import { getPageSummaryComments, getPageSummaryContent } from '@/content-script/utils'
import { commentSummaryPrompt, pageSummaryPrompt, pageSummaryPromptHighlight } from '@/utils/prompt'
import { isIOS } from '@/utils/utils'
import Draggable from 'react-draggable'
import ChatUi from './sidebar/chat/ChatUi'
import Sidebar from './sidebar/Sidebar'

interface Props {
  pageSummaryEnable: boolean
  pageSummaryWhitelist: string
  pageSummaryBlacklist: string
  siteRegex: RegExp
}

function PageSummary(props: Props) {
  const { pageSummaryEnable, pageSummaryWhitelist, pageSummaryBlacklist, siteRegex } = props
  const [showCard, setShowCard] = useState(false)
  const [supportSummary, setSupportSummary] = useState(true)
  const [question, setQuestion] = useState('')
  const [loading, setLoading] = useState(false)
  const [show, setShow] = useState<boolean>(false)
  const [isSidebarVisible, setIsSidebarVisible] = useState(false)
  const [isDragging, setIsDragging] = useState(false)

  const toggleSidebar = () => {
    setIsSidebarVisible((prevState) => !prevState)
  }

  // Function that fires when the floating icon is clicked
  const onSwitch = useCallback(() => {
    setIsSidebarVisible((prevState) => {
      const currentState = !prevState

      if (currentState) {
        setQuestion('')
        setLoading(false)
      }

      return currentState
    })
    console.log(isSidebarVisible)
  }, [])

  const openOptionsPage = useCallback(() => {
    Browser.runtime.sendMessage({ type: 'OPEN_OPTIONS_PAGE' })
  }, [])

  const onSummary = useCallback(async () => {
    setLoading(true)
    setSupportSummary(true)

    setQuestion('')

    const pageComments = await getPageSummaryComments()
    const pageContent = await getPageSummaryContent()
    const article = pageComments ? pageComments : pageContent

    const title = article?.title || document.title || ''
    const description =
      article?.description ||
      document.querySelector('meta[name="description"]')?.getAttribute('content') ||
      ''
    const content = article?.content ? description + article?.content : title + description

    if (article?.content || description) {
      const language = window.navigator.language
      const userConfig = await getUserConfig()
      const providerConfigs = await getProviderConfigs()

      const promptContent = getSummaryPrompt(
        content.replace(/(<[^>]+>|\{[^}]+\})/g, ''),
        providerConfigs.provider,
      )
      const replyLanguage = userConfig.language === Language.Auto ? language : userConfig.language

      const prompt = pageComments?.content
        ? commentSummaryPrompt({
            content: promptContent,
            language: replyLanguage,
            prompt: userConfig.promptComment
              ? userConfig.promptComment
              : pageSummaryPromptHighlight,
            rate: article?.['rate'],
          })
        : pageSummaryPrompt({
            content: promptContent,
            language: replyLanguage,
            prompt: userConfig.promptPage ? userConfig.promptPage : pageSummaryPromptHighlight,
          })

      setQuestion(prompt)
      return
    }

    setSupportSummary(false)
  }, [])

  useEffect(() => {
    Browser.runtime.onMessage.addListener((message) => {
      const { type } = message
      if (type === 'OPEN_WEB_SUMMARY') {
        if (showCard) {
          return
        }

        setQuestion('')
        setShowCard(true)
        setLoading(false)
      }
    })
  }, [showCard])

  useEffect(() => {
    const hostname = location.hostname
    const blacklist = pageSummaryBlacklist.replace(/[\s\r\n]+/g, '')
    const whitelist = pageSummaryWhitelist.replace(/[\s\r\n]+/g, '')

    const inWhitelist = !whitelist
      ? !blacklist.includes(hostname)
      : !blacklist.includes(hostname) && pageSummaryWhitelist.includes(hostname)

    const show =
      pageSummaryEnable && ((isIOS && inWhitelist) || (inWhitelist && !siteRegex?.test(hostname)))

    setShow(show)
  }, [pageSummaryBlacklist, pageSummaryEnable, pageSummaryWhitelist, siteRegex])

  const handleDrag = () => {
    setIsDragging(true)
  }

  const handleDragEnd = () => {
    setTimeout(() => setIsDragging(false), 0)
  }

  const handleClick = (event) => {
    if (isDragging) {
      // Prevent click when dragging
      event.preventDefault()
    } else {
      onSwitch()
    }
  }

  return (
    <>
      {showCard ? (
        <div className="glarity--card">
          <div className="glarity--card__head ">
            <div className="glarity--card__head--title">
              <a href="https://glarity.app" rel="noreferrer" target="_blank">
                <img src={logo} alt={APP_TITLE} /> {APP_TITLE}
              </a>{' '}
              <button
                className={classNames('glarity--btn', 'glarity--btn__icon')}
                onClick={openOptionsPage}
              >
                <GearIcon size={14} />
              </button>
            </div>

            <div className="glarity--card__head--action">
              <button
                className={classNames('glarity--btn', 'glarity--btn__icon')}
                onClick={onSwitch}
              >
                <XCircleFillIcon />
              </button>
            </div>
          </div>

          <div className="glarity--card__content">
            {question ? (
              <div className="glarity--container">
                <div className="glarity--chatgpt">
                  <ChatGPTQuery question={question} />
                  <ChatUi />
                </div>
              </div>
            ) : (
              <div className="glarity--card__empty ">
                {!supportSummary ? (
                  'Sorry, the summary of this page is not supported.'
                ) : (
                  <button
                    className={classNames(
                      'glarity--btn',
                      'glarity--btn__primary',
                      // 'glarity--btn__large',
                      'glarity--btn__block',
                    )}
                    onClick={onSummary}
                    disabled={loading}
                  >
                    Summary
                  </button>
                )}
              </div>
            )}
          </div>
        </div>
      ) : (
        show && (
          // Floating icon on webpages
          <Draggable axis="y" bounds={{ right: 0 }} onDrag={handleDrag} onStop={handleDragEnd}>
            <button
              onClick={handleClick}
              className={classNames(
                'glarity--btn',
                'glarity--btn__launch',
                'glarity--btn__primary',
              )}
            >
              <img
                src={logoWhite}
                alt={APP_TITLE}
                className="glarity--w-5 glarity--h-5 glarity--rounded-sm"
                draggable={false} // The user is likely trying to drag the button not the image itself
              />
            </button>
          </Draggable>
        )
      )}
      {isSidebarVisible && <Sidebar toggleSidebar={toggleSidebar} />}
    </>
  )
}

export default PageSummary
