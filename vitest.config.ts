import { resolve } from 'path'
import { defineConfig } from 'vitest/config'

export default defineConfig({
  test: {
    globals: true,
    environment: 'jsdom',
    setupFiles: 'tests/setup.ts',
    alias: {
      // Mock browser extension context for testing
      'webextension-polyfill': resolve(__dirname, './tests/mocks/webextension-polyfill.js'),
      '@': resolve(__dirname, './src'),
    },
  },
})
